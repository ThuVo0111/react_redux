import InputComponent from './components/InputComponent';
import './App.css';
import ListComponent from './components/ListComponent';// import IframeComponent from './components/IframeComponent';

function App() {
  return (
    <div className="App">
     <InputComponent/>
     <ListComponent/>
    </div>
  );
}

export default App;
