import { createSlice, configureStore } from "@reduxjs/toolkit";
const initialState = {links:[]};
const linkSlice = createSlice({
    name: 'link', 
    initialState: initialState,
    reducers: {
      submitLink(state, action) {
        state.links.push(action.payload); 
      },
    }
  });
const store = configureStore({
    reducer: linkSlice.reducer,
})
export const linkActions = linkSlice.actions;
export default store;
