import React from 'react';

const IframeComponent = ({ link }) => {
  return (
    <div>
      <h2>Selected social network link</h2>
      <iframe
        src={link}
        width="560"
        height="315"
        title="Embedded Video"
        allowFullScreen
      ></iframe>
    </div>
  );
};

export default IframeComponent;