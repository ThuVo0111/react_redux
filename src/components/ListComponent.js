import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import IframeComponent from './IframeComponent';

const ListComponent = () => {
  const links = useSelector(state => state.links);
  const [selectedLink, setSelectedLink] = useState(null);

  const handleClick = (link) => {
    setSelectedLink(link);
  };
  const reversedLinks = [...links].reverse();

  return (
    <div>
      <h2>List of links from 03 Social Networks</h2>
      {reversedLinks.map((link, index) => (
        <li key={index} onClick={() => handleClick(link)}>
          {link}
        </li>
      ))}
      {selectedLink && <IframeComponent link={selectedLink} />}
    </div>
  );
};

export default ListComponent;



