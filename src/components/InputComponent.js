
import { useState } from 'react';
import { linkActions } from '../store/index';
import {useDispatch } from 'react-redux';

const InputComponent = () => {
  const [inputValue, setInputValue] = useState('');
   const dispatch = useDispatch();
  const handleChange = (e) => {
    setInputValue(e.target.value);
  }

  const handleSubmit = () => {
    if (inputValue.includes('youtube.com') || inputValue.includes('tiktok.com') || inputValue.includes('instagram.com')) {
      dispatch(linkActions.submitLink(inputValue));
      setInputValue("");
    } else {
      alert('Invalid input. Please enter a valid social media link.')
    }
  }
  return (
    <div>
      <h2> Input link from 3 Social Networks <br/>- Youtube/Tiktok/Instagram</h2>
      <input type="text" value={inputValue} onChange={handleChange}/>
      <button onClick={handleSubmit}>Submit</button>
    </div>
  );
};
export default InputComponent;
